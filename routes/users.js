var express = require('express');
var router = express.Router();

const middleware = require('../middlewares/users')
const controller = require('../controllers/users');
const { route } = require('.');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


// router.post('/cadastro', 
//   middleware.postUserData
// );



router.post('/login',

  middleware.checkLogin, //processamento da funcionalidade
  
  controller.responseCheckLogin //resposta enviada ao usuário

);


// router.get('/array', function(req, res, next) {
//   res.status(200).json([{ "id": 123 }  , { "id":223  }]);
// });


module.exports = router;
