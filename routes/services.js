const multer = require("multer");
var express = require('express');
var router = express.Router();

const servicesMid = require('../middlewares/services')
const authMid = require('../middlewares/utils/auth')
//crie uma variável de armazenamento e indique a ela que o armazenamento é no HD
var storage = multer.diskStorage({

  destination: function (req, file, cb) {
    cb(null, 'C:\\Users\\andre\\OneDrive\\Documentos\\Node Apps\\UC-WEB\\servGendaProject\\serverData')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname) //Appending .jpg
  }

});

var upload = multer({ storage: storage });

router.post('/',
  
  upload.single('image'),
  authMid.authorization,
  servicesMid.postService
)

router.get('/:serviceID',

  servicesMid.getService
)


module.exports = router;