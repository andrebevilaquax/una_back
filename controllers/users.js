
function responsePostUser(req, res, next) {

    if (res.locals.data !== null && typeof res.locals.data !== 'undefined') {
        res.status(200).send(res.locals.data.result);
      } else {
        res.status(400).send({
          message: 'Erro durante solicitação de cadastro de usuário',
        })
      }
  }


  function responseCheckLogin(req,res,next){

    
    if (res.locals.data.result !== null && typeof res.locals.data.result !== 'undefined') {
      res.status(200).send({token:res.locals.data.result, tipo:res.locals.data.tipo});
    } 
    else {
      res.status(400).send({
        message:  res.locals.data.Message
      })
    }



  }
module.exports = {responsePostUser, responseCheckLogin}