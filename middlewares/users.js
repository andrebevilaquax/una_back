const errorManagement = require('../middlewares/utils/errorManagment');
const userModel = require('../models/users')
var jwt = require('jsonwebtoken');


async function postUserData(req, res, next) {
    console.log("Entrou aqui")
    if (req.body.login === null || typeof req.body.login === 'undefined') {
        next(errorManagement.createError(
            'MISSING_LOGIN',
            404,
            'Não foi informado um login.'
        ));
        return;
    }
    else if (req.body.senha === null || typeof req.body.senha === 'undefined') {
        next(errorManagement.createError(
            'MISSING_SENHA',
            400,
            'Não foi informado uma senha.'
        ));
        return;
    }
    else if (req.body.tipo === null || typeof req.body.tipo === 'undefined') {
        next(errorManagement.createError(
            'MISSING_TYPE',
            400,
            'Não foi informado um tipo de cliente.'
        ));
        return;
    }
    else if (req.body.cpf === null || typeof req.body.cpf === 'undefined') {
        next(errorManagement.createError(
            'MISSING_CPF',
            400,
            'Não foi informado o cpf.'
        ));
        return;
    }
    else if (req.body.nome === null || typeof req.body.nome === 'undefined') {
        next(errorManagement.createError(
            'MISSING_NAME',
            400,
            'Não foi informado o nome do cliente.'
        ));
        return;
    }

    try {
        let user = await userModel.create(req.body)
        if (!user) {
            next(errorManagement.createError(
                'Problema na inserção do usuário',
                400,
                'Não foi possível realizar o cadastro do usuário.'
            ));
            return;
        }
        else {
            res.locals.data = {
                result: user
            };
            next();
        }

    }
    catch (error) {
        console.log(error)
        next(errorManagement.createError(
            'USER_DATABASE_ERROR',
            401,
            'Problema no acesso ao banco de dados durante o cadastro de usuário.'
        ));
    }
}


async function checkLogin(req, res, next) {
    console.log("Os dados do request chegaram no middleware ", req.body);
    
    let resultadoBanco; 
    try {
        resultadoBanco = await userModel.findOne({ "login": req.body.login});
        
        if(resultadoBanco){

            if(resultadoBanco.senha == req.body.senha ){

                var token = jwt.sign(
                    
                    { 
                        "type": resultadoBanco.tipo, 
                        "cpf": resultadoBanco.cpf, 
                        exp: Math.floor(Date.now() / 1000) + (60 * 60) 
                    }, 'palavraSuperSecreta');

                res.locals.data = { result: token, tipo: resultadoBanco.tipo };
                return next();

            }
            else{
                res.locals.data = {"Message":"Senha não corresponde ao informado"}
                return next()
            }

        }
        else{
            res.locals.data = {"Message":"Usuário não encontrado"}
            return next()
        }

    }
    catch (error) {
        res.locals.data = {"Message":"Internal server error"}
        return next()
    }




}

module.exports = { postUserData, checkLogin }