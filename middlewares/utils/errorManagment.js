function createError(name, status, message) {
    let error = new Error();
    error.name = name;
    error.message = message;
    error.status = status;
  
    return error;
  }
  
  module.exports = {
    createError
  }
  