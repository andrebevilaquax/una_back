function handleRequestErrors() {
    return function (error, request, response, next) {
      if (error.status === null || typeof error.status === 'undefined') {
        next(error);
      }
  
      console.error(`[ ${new Date().toISOString()} ] ${error.name} - ${error.message}`);
      response.status(error.status).send({message: error.message});
    }
  }
  
  module.exports = {
    handleRequestErrors
  }