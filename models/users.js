const mongoose = require('mongoose');
const Schema = mongoose.Schema; //a biblioteca mongoose tem o schema para permitir tipagem dos dados e modelagem

const usersModel = new Schema(
    {
        login : {
            type: String,
            required: true
        },
        senha : {
            type: String,
            required: true
        },
        nome :  {
            type: String,
            required: true
        },
        tipo :  {
            type: String,
            required: true
        },
        cpf : {
            type: String,
            required: true
        }
    }, 
    { _id: true, collection: 'users' } //aqui estamos nomeando nossa collection e dizendo que o id será criado automaticamente
);
    
module.exports = mongoose.model('users', usersModel); //aqui estamos exportando nosso modelo, para que ele possa ser usado nos outros arquivos