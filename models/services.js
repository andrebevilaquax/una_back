const mongoose = require('mongoose');
const Schema = mongoose.Schema; //a biblioteca mongoose tem o schema para permitir tipagem dos dados e modelagem

const servicesModel = new Schema(
    {
        descricao: {
            type: String,
            required: true
        },
        categoria: {
            type: String,
            required: true
        },
        subcategorias: [{
            descricao: {
                type: String,
                required: true
            }, 
            imgURL: {
                type: String,
                required: true
            }
        }],
        imgURL: {
            type: String,
            required: true
        }
    },
    { _id: true, collection: 'services' } //aqui estamos nomeando nossa collection e dizendo que o id será criado automaticamente
);

module.exports = mongoose.model('services', servicesModel); //aqui estamos exportando nosso modelo, para que ele possa ser usado nos outros arquivos